/*
Implementation of the Rubberband Rectangle program, in reference
to the provided powerpoint slides.
*/
#include <stdlib.h>
#include <stdio.h>
#include <GL/glut.h>

/* Global variables */
GLsizei wh = 500, ww = 500; /* initial window size */
GLfloat size = 3.0;   /* half side length of square */
GLfloat Xo, Yo, Xf, Yf; /*initial and final corners of rectangle*/
GLint pntId = 0; /*point Id: 0=first point, 1=second point*/

///* reshaping routine called whenever window is resized or moved */

void myReshape(GLsizei w, GLsizei h)
{
	/* adjust clipping box */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, (GLdouble)w, 0.0, (GLdouble)h, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//* adjust viewport and clear */
	glViewport(0, 0, w, h);

	//* set global size for use by drawing routine */
	ww = w;
	wh = h;
	printf("%d ", ww);
	printf("%d \n", wh);
}

void myinit()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glViewport(0, 0, ww, wh);

	/* Pick 2D clipping window to match
	size of screen window. This choice avoids having to scale object
	coordinates each time window is resized. */

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, (GLdouble)ww, 0.0, (GLdouble)wh, -1.0, 1.0);
}

void mouse(int btn, int state, int x, int y)
{
	//To determine the first point of rectangle
	if (btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		Xo = x;//(GLfloat)x / ww;
		Yo = wh - y;//(GLfloat)(wh - y) / wh;
		printf("%f %f \n", Xo, Yo);
		glColor3f(0.1, 0.3, 0.0);
		glLogicOp(GL_XOR); //Enable erasing by twice-drawing
		pntId = 0; //To signal that the first point is placed
	}

	//To determine the second point of rectangle
	if (btn == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		glRectf(Xo, Yo, Xf, Yf);
		glFlush();
		glColor3f(0.0, 0.1, 0.2);
		Xf = x;//(GLfloat)x / ww;
		Yf = wh - y;//(GLfloat)(wh - y) / wh;
		printf("%f %f \n", Xo, Yo);
		glLogicOp(GL_COPY); //draws rectangle twice
		glRectf(Xo, Yo, Xf, Yf);
		glFlush();
	}
}

void move(int x, int y)
{
	glEnable(GL_COLOR_LOGIC_OP);
	if (pntId == 1)
	{
		glRectf(Xo, Yo, Xf, Yf);
		glFlush();
	}

	Xf = x;//(GLfloat)x / ww;
	Yf = wh - y;//(GLfloat)(wh - y) / wh;
	//printf("%f %f \n", x, y);
	glRectf(Xo, Yo, Xf, Yf);
	glFlush();
	pntId = 1;
}


/* display callback required by GLUT */
void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glFlush();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(ww, wh);
	glutCreateWindow("Rubberband Rectangle");
	myinit();
	glutReshapeFunc(myReshape);
	glutMouseFunc(mouse);
	glutMotionFunc(move);
	glutDisplayFunc(display);
	glutMainLoop();
}
